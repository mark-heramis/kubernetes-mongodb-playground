# Kubernetest Playground with Mongo Express

# Prerequisites

- install [minikube](https://minikube.sigs.k8s.io/docs/start/)
- install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)


# Objective

1. Create a [MongoDB](https://hub.docker.com/_/mongo) database [pod](https://kubernetes.io/docs/concepts/workloads/pods/)
2. Create a [Mongo-Express](https://hub.docker.com/_/mongo-express) [pod](https://kubernetes.io/docs/concepts/workloads/pods/)
3. the MongoDB instance must not be available for the public

# Files

- **mongo-secret.yaml**: a [kubernetes secret](https://kubernetes.io/docs/concepts/configuration/secret/) file used for storing confidential information such as **username** and/or **passwords**
- **mongo.yaml**: Create a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and a [Service](https://kubernetes.io/docs/concepts/services-networking/service/) for the [MongoDB](https://hub.docker.com/_/mongo) database that will only be used internally.
- **mongo-configmap.yaml**: create a [configMap](https://kubernetes.io/docs/concepts/configuration/configmap/) used to store reusable configuration files to store configurations that are not necessarily confidential information.
- **mongo-express.yaml**: Create a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and a [Service](https://kubernetes.io/docs/concepts/services-networking/service/) for the [Mongo-Express](https://hub.docker.com/_/mongo-express) that will connect to the **internal** MongoDB instance and is also public facing.

# Commands

This commands will create/update the `secret`, `configmap`, `deployment` and `service`.
```bash
kubectl apply -f mongo-secret.yaml
kubectl apply -f mongo.yaml
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongo-express.yaml
```
Since we're using the minikube version (offline version) of kubernetes, the `mongo-express-service` (which is the service we created in the `mongo-express.yaml`) should not be getting a public IP address (you can only get that when its online).

You can verify this with the command below

```bash
kubectl get service
```

after this command, you should get a list of services, and the `mongo-express-service` service should not have an `EXTERNAL-IP`.

However to see if things are propperly working, you can emmulate a public IP with the following command:

```bash
minikube service mongo-express-service
```

**Also**... check this [kubernetes cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) and [minikube cheatsheet](https://cheatsheet.dennyzhang.com/cheatsheet-minikube-a4) document, it will be useful.